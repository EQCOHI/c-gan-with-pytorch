import torch
import matplotlib.pyplot as plt
import numpy as np
import os
import torch.nn as nn
from torchvision.datasets import ImageFolder, MNIST
from torchvision import transforms
from torch import autograd
from torch.autograd import Variable
from torchvision.utils import make_grid
from tensorboardX import SummaryWriter
import argparse
from accuracy_block import test
from accuracy_block import Net

transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize([0.5],[0.5])
])

batch_size = 32
data_loader = torch.utils.data.DataLoader(MNIST('data', train=True, download=True, transform=transform), batch_size=batch_size, shuffle=True)

class Discriminator(nn.Module):
    def __init__(self):
        super().__init__()

        # nn.Embedding(총 단어의 갯수, 임베딩 시킬 벡터의 차원)
        # Embedding 모듈은 index를 표현하는 LongTensor를 input, 따라서 원핫벡터로 명시적으로 바꿔주지 않아도 됨
        self.label_emb = nn.Embedding(10, 10)

        self.model = nn.Sequential(
            nn.Linear(794, 1024),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Dropout(0.3),
            nn.Linear(1024, 512),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Dropout(0.3),
            nn.Linear(512, 256),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Dropout(0.3),
            nn.Linear(256, 1),
            nn.Sigmoid()
        )

    def forward(self, x, labels):
        x = x.view(x.size(0), 784)
        c = self.label_emb(labels)
        x = torch.cat([x, c], 1)
        out = self.model(x)
        #torch.squeeze() -> 차원변경?
        return out.squeeze()

class Generator(nn.Module):
    def __init__(self):
        super().__init__()

        self.label_emb = nn.Embedding(10, 10)

        self.model = nn.Sequential(
            nn.Linear(110, 256),
            nn.LeakyReLU(0.2 , inplace=True),
            nn.Linear(256, 512),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Linear(512, 1024),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Linear(1024, 784),
            nn.Tanh()
        )

    def forward(self, z, labels):
        z = z.view(z.size(0), 100)
        c = self.label_emb(labels)
        x = torch.cat([z, c], 1)
        out = self.model(x)

        return out.view(x.size(0), 28,28)

def generator_train_step(batch_size, discriminator, generator, g_optimizer, criterion):
    g_optimizer.zero_grad()
    z = Variable(torch.randn(batch_size, 100)).cuda()
    fake_labels = Variable(torch.LongTensor(np.random.randint(0, 10, batch_size))).cuda()
    fake_images = generator(z, fake_labels)
    validity = discriminator(fake_images, fake_labels)
    g_loss = criterion(validity, Variable(torch.ones(batch_size)).cuda())
    g_loss.backward()
    g_optimizer.step()

    return g_loss.item()

def discriminator_train_step(batch_size, discriminator, generator, d_optimizer, criterion, real_images, labels):
    d_optimizer.zero_grad()

    #train with real images
    real_validity = discriminator(real_images, labels)
    real_loss = criterion(real_validity, Variable(torch.ones(batch_size)).cuda())

    #train with fake images
    z = Variable(torch.randn(batch_size, 100)).cuda()
    #np.random.randint -> 0~10 number random generate
    fake_labels = Variable(torch.LongTensor(np.random.randint(0, 10, batch_size)).cuda())
    fake_images = generator(z, fake_labels)
    fake_validity = discriminator(fake_images, fake_labels)
    fake_loss = criterion(fake_validity, Variable(torch.zeros(batch_size)).cuda())

    d_loss = real_loss + fake_loss
    d_loss.backward()
    d_optimizer.step()
    return d_loss.item()

if __name__ == '__main__':
    # Train Setting
    parser = argparse.ArgumentParser(description="Pytorch CGAN MNIST")
    parser.add_argument("--batch-size", type=int, default='10', metavar='N'
                        , help='input batch size for training')
    parser.add_argument('--epochs', type=int, default='5', metavar='N'
                        , help='number of epochs (default:5)')
    parser.add_argument('--save-model', action='store_true', default=False,
                        help='For saving the current model')
    parser.add_argument('--test', action='store_true', default=True,
                        help='For Creating number to test(default:False)')
    parser.add_argument('--train', action='store_true', default=False,
                        help='Training the CGAN model')
    parser.add_argument('--create-number', type=int, default=4,
                        help="Creating your number")
    args = parser.parse_args()

    generator = Generator().cuda()
    discriminator = Discriminator().cuda()

    criterion = nn.BCELoss()
    d_optimizer = torch.optim.Adam(discriminator.parameters(), lr=1e-4)
    g_optimizer = torch.optim.Adam(generator.parameters(), lr=1e-4)

    writer = SummaryWriter()

    #num_epochs = 20
    n_critic = 5
    display_step = 50

    if args.train:
        for epoch in range(args.epochs):
            print("Starting epoch {}...".format(epoch), end=' ')

            for i, (images, labels) in enumerate(data_loader):
                step = epoch * len(data_loader) + i + 1
                real_images = Variable(images).cuda()
                labels = Variable(labels).cuda()
                generator.train()

                d_loss = discriminator_train_step(len(real_images), discriminator,
                                                  generator, d_optimizer, criterion,
                                                  real_images, labels)
                g_loss = generator_train_step(batch_size, discriminator, generator, g_optimizer, criterion)

                writer.add_scalars('scalars', {'g_loss': g_loss, 'd_loss': d_loss}, step)

                if step % display_step == 0:
                    generator.eval()
                    z = Variable(torch.randn(9, 100)).cuda()
                    labels = Variable(torch.LongTensor(np.arange(9))).cuda()
                    sample_images = generator(z, labels).unsqueeze(1)
                    grid = make_grid(sample_images, nrow=3, normalize=True)
                    writer.add_image('sample_image', grid, step)

            print('Done!')
        if args.save_model:
            print("******************saved generator model*********************")
            model_to_save = generator.module if hasattr(generator, 'module') else generator
            torch.save(model_to_save.state_dict(), 'generator_state{}.pt'.format(args.epochs))
            # torch.save(generator.state_dict(), 'generator_state.pt')

        z = Variable(torch.randn(100, 100)).cuda()
        labels = torch.LongTensor([i for i in range(10) for _ in range(10)]).cuda()

        # print("labels: ",labels)

        images = generator(z, labels).unsqueeze(1)

        print(images.shape)

        grid = make_grid(images, nrow=10, normalize=True)

        fig, ax = plt.subplots(figsize=(10, 10))
        ax.imshow(grid.permute(1, 2, 0).data.cpu(), cmap='binary')
        ax.axis('off')
        plt.show()

    if args.test:
        correct = 0

        generator.load_state_dict(torch.load("C:\\Users\\choii\\PycharmProjects\\cgan\\epoch5.pt", map_location='cuda'))

        print("************Loaded checkpoint generate model************")

        for i in range(1,100):
            z = Variable(torch.randn(1, 100)).cuda()
            label = torch.LongTensor([args.create_number]).cuda()

            img = generator(z, label).data.cpu()
            img = 0.5*img + 0.5

            # img = transforms.ToPILImage(img)
            img_ = img.numpy()
            img_ = np.reshape(img_,(28,28))
            img__ = np.reshape(img_,(1,1,28,28))
            img__ = torch.from_numpy(img__)
            # print(img_.shape)
            acc_model = Net()
            acc_model.load_state_dict(torch.load("C:\\Users\\choii\\PycharmProjects\\cgan\\mnist_cnn.pt", map_location='cuda'))
            output = acc_model(img__)
            pred = output.argmax(dim=1, keepdim=True) # gent the index of the max log-prob
            pred = pred.numpy()
            if pred == args.create_number:
                correct = correct + 1

        print("Accuracy:{}".format(correct/100))

        #show the one image

        # plt.title("Create number: {}".format(args.create_number))
        # plt.imshow(img_, cmap='gray')
        # plt.xlabel("pred: {}, label: {}".format(pred, args.create_number))
        # plt.show()